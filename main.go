package main

import (
	"./handlers/http"
	"./migrations"
)

func main() {
	migrations.Migrate()
	http.Init()
}
