package controllers

import (
	"../migrations"
	"../model"
	"encoding/json"
	"net/http"
)

func ListReceipts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")

	var receipts []migrations.Receipt

	receipts, err := model.ListReceipts()

	if len(err) > 0 {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err)
		return
	}

	json.NewEncoder(w).Encode(receipts)

}

func AddReceipt(w http.ResponseWriter, r *http.Request) {
	var receipt migrations.Receipt
	_ = json.NewDecoder(r.Body).Decode(&receipt)

	receipt, err := model.CreateReceipt(receipt)
	w.Header().Set("Content-type", "application/json")

	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err.Error())
		return
	}

	json.NewEncoder(w).Encode(receipt)
}
