package controllers

type Error struct {
	Number  string `json:"number"`
	Message string `json:"message"`
}
