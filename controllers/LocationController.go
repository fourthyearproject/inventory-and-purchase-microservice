package controllers

import (
	"../migrations"
	"../model"
	"encoding/json"
	"net/http"
)

func AddLocation(w http.ResponseWriter, r *http.Request) {
	defer recover()

	var location migrations.Location
	_ = json.NewDecoder(r.Body).Decode(&location)

	location, err := model.AddLocation(location)
	w.Header().Set("Content-type", "application/json")

	if len(err) > 0 {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err)
		return
	}

	json.NewEncoder(w).Encode(location)
}

func UpdateLocation(w http.ResponseWriter, r *http.Request) {

	var location migrations.Location
	_ = json.NewDecoder(r.Body).Decode(&location)

	location, err := model.UpdateLocation(location)
	w.Header().Set("Content-type", "application/json")

	if len(err) > 0 {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err)
		return
	}

	json.NewEncoder(w).Encode(location)
}

func ListLocations(w http.ResponseWriter, r *http.Request) {

	var locations []migrations.Location
	locations, err := model.ListLocations()
	w.Header().Set("Content-type", "application/json")
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err)
		return
	}

	json.NewEncoder(w).Encode(locations)
}
