package controllers

import (
	"../config"
	"../keys"
	"../migrations"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func GetUser(user_id uint, token string) migrations.User {
	url := fmt.Sprintf("%suser/%d", config.AuthMsURL, user_id)

	client := &http.Client{}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("X-api-key", keys.AUTH_API_KEY)
	req.Header.Add("Authorization", token)
	response, _ := client.Do(req)
	defer response.Body.Close()

	contents, _ := ioutil.ReadAll(response.Body)

	var user migrations.User
	json.Unmarshal(contents, &user)

	return user
}
