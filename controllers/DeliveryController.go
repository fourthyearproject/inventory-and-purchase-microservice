package controllers

import (
	"../migrations"
	"../model"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func CreateDelivery(w http.ResponseWriter, r *http.Request) {
	var delivery migrations.Delivery
	_ = json.NewDecoder(r.Body).Decode(&delivery)

	var errors map[string]string

	errors = make(map[string]string)

	w.Header().Set("Content-type", "application/json")

	if delivery.SupplierID == 0 {
		errors["supplier_id"] = "Supplier was not provided"
	}

	if len(errors) > 0 {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(errors)
		return
	}

	delivery, err := model.CreateDelivery(delivery)
	w.Header().Set("Content-type", "application/json")

	if len(err) > 0 {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err)
		return
	}
	supplier := GetUser(delivery.SupplierID, r.Header.Get("Authorization"))

	var deliveryInfo migrations.DeliveryInfo

	deliveryInfo.Supplier = supplier
	deliveryInfo.ID = delivery.ID
	deliveryInfo.CreatedAt = delivery.CreatedAt
	deliveryInfo.UpdatedAt = delivery.UpdatedAt
	deliveryInfo.Status = delivery.Status

	json.NewEncoder(w).Encode(deliveryInfo)

}

func ListDeliveriesBySupplier(w http.ResponseWriter, r *http.Request)  {

	vars := mux.Vars(r)

	id, _ := strconv.Atoi(vars["id"])
	var deliveries []migrations.Delivery

	deliveries, err := model.ListDeliveriesBySupplier(id)

	w.Header().Set("Content-type", "application/json")

	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err.Error())
		return
	}

	var deliveriesInfo []migrations.DeliveryInfo

	for _, delivery := range deliveries {
		var deliveryInfo migrations.DeliveryInfo

		deliveryInfo.Supplier = GetUser(delivery.SupplierID, r.Header.Get("Authorization"))
		deliveryInfo.ID = delivery.ID
		deliveryInfo.CreatedAt = delivery.CreatedAt
		deliveryInfo.UpdatedAt = delivery.UpdatedAt
		deliveryInfo.Status = delivery.Status

		deliveriesInfo = append(deliveriesInfo, deliveryInfo)
	}

	json.NewEncoder(w).Encode(deliveriesInfo)
}

func ListDeliveries(w http.ResponseWriter, r *http.Request) {
	var deliveries []migrations.Delivery

	deliveries, err := model.ListDeliveries()

	w.Header().Set("Content-type", "application/json")

	if len(err) > 0 {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err)
		return
	}

	var deliveriesInfo []migrations.DeliveryInfo

	for _, delivery := range deliveries {
		var deliveryInfo migrations.DeliveryInfo

		deliveryInfo.Supplier = GetUser(delivery.SupplierID, r.Header.Get("Authorization"))
		deliveryInfo.ID = delivery.ID
		deliveryInfo.CreatedAt = delivery.CreatedAt
		deliveryInfo.UpdatedAt = delivery.UpdatedAt
		deliveryInfo.Status = delivery.Status

		deliveriesInfo = append(deliveriesInfo, deliveryInfo)
	}

	json.NewEncoder(w).Encode(deliveriesInfo)
}

func AddDeliveryItem(w http.ResponseWriter, r *http.Request) {
	var item migrations.Inventory
	json.NewDecoder(r.Body).Decode(&item)

	item.Direction = "in"

	item, err := model.AddDeliveryItem(item)

	w.Header().Set("Content-type", "application/json")

	if len(err) > 0 {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err)
		return
	}
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(item)
}

func ListStock(w http.ResponseWriter, r *http.Request) {

	var stock []migrations.Stock

	stock, err := model.FetchStock()

	w.Header().Set("Content-type", "application/json")
	if len(err) > 0 {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err)
		return
	}

	json.NewEncoder(w).Encode(stock)
}

func DeliveryDetails(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, _ := strconv.Atoi(vars["id"])
	var delivery migrations.Delivery

	delivery, err := model.DeliveryDetails(id)

	w.Header().Set("Content-type", "application/json")

	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err.Error())
		return
	}
	var deliveryInfo migrations.DeliveryInfo

	deliveryInfo.Supplier = GetUser(delivery.SupplierID, r.Header.Get("Authorization"))
	deliveryInfo.ID = delivery.ID
	deliveryInfo.CreatedAt = delivery.CreatedAt
	deliveryInfo.UpdatedAt = delivery.UpdatedAt
	deliveryInfo.Status = delivery.Status
	deliveryInfo.Inventories = delivery.Inventories
	deliveryInfo.Receipts = delivery.Receipts

	json.NewEncoder(w).Encode(deliveryInfo)
}
