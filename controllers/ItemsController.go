package controllers

import (
	"../migrations"
	"../model"
	"encoding/json"
	"net/http"
)

func ListItems(w http.ResponseWriter, r *http.Request) {
	items, err := model.ListItems()

	w.Header().Set("Content-type", "application/json")

	if len(err) > 0 {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err)
		return
	}

	json.NewEncoder(w).Encode(items)

}

func AddItems(w http.ResponseWriter, r *http.Request) {
	var item migrations.Item
	_ = json.NewDecoder(r.Body).Decode(&item)

	var errors map[string]string

	errors = make(map[string]string)

	w.Header().Set("Content-type", "application/json")

	if item.Name == "" {
		errors["name"] = "Item name is required"
	}

	if item.Unit == "" {
		errors["unit"] = "Unit name is require"
	}

	if item.SmallestUnit == 0.0 {
		errors["smallest_unit"] = "Smallest unit is required"
	}

	if len(errors) > 0 {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(errors)
		return
	}

	item, err := model.AddItem(item)

	if len(err) > 0 {
		w.WriteHeader(http.StatusForbidden)
		json.NewEncoder(w).Encode(err)
		return
	}

	json.NewEncoder(w).Encode(item)
}
