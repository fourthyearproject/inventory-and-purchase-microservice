package config

const (
	Name        = "Inventory And Purchase"
	Description = "Inventory and Purchase Micro service"

	AuthMsURL = "http://localhost:8181/"

	SUPER_ADMIN_ID  = 1
	ADMIN_ID        = 2
	HR_ID           = 3
	STORE_KEEPER_ID = 4
	SUPPLIER_ID     = 5
	USER_CLIENT     = 6
)
