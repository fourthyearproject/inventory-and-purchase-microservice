package model

import "../migrations"
import "../database"

type Project struct {
}

func AddItem(item migrations.Item) (migrations.Item, []error) {
	db := database.Connect()
	db.LogMode(false)
	err := db.Create(&item).GetErrors()

	return item, err
}

func ListItems() ([]migrations.Item, []error) {
	db := database.Connect()

	var items []migrations.Item
	err := db.Find(&items).GetErrors()

	return items, err
}

func ListReceipts() ([]migrations.Receipt, []error) {
	db := database.Connect()

	var receipts []migrations.Receipt
	err := db.Preload("Delivery").Find(&receipts).GetErrors()

	return receipts, err
}

func CreateReceipt(receipt migrations.Receipt) (migrations.Receipt, error) {
	db := database.Connect()

	err := db.Create(&receipt).Error

	return receipt, err
}

func CreateDelivery(delivery migrations.Delivery) (migrations.Delivery, []error) {

	db := database.Connect()

	err := db.Create(&delivery).GetErrors()

	return delivery, err

}

func ListDeliveries() ([]migrations.Delivery, []error) {

	db := database.Connect()

	var deliveries []migrations.Delivery

	err := db.Preload("Receipts").Find(&deliveries).GetErrors()

	return deliveries, err
}

func ListDeliveriesBySupplier(supplier_id int) ([]migrations.Delivery, error) {
	db := database.Connect()

	var deliveries []migrations.Delivery

	err := db.Preload("Receipts").Preload("Inventories.Item").Where("supplier_id = ?", supplier_id).Find(&deliveries).Error

	return deliveries, err
}

func DeliveryDetails(delivery_id int) (migrations.Delivery, error) {
	db := database.Connect()

	var deliveries migrations.Delivery

	err := db.Preload("Receipts").Preload("Inventories.Item").First(&deliveries, delivery_id).Error

	return deliveries, err
}

func AddDeliveryItem(item migrations.Inventory) (migrations.Inventory, []error) {

	db := database.Connect()

	errors := db.Create(&item).GetErrors()

	if errors == nil {
		UpdateInventory(item)
	}
	return item, errors
}

func UpdateInventory(item migrations.Inventory) {

}

func FetchStock() ([]migrations.Stock, []error) {

	db := database.Connect()

	var stock []migrations.Stock

	err := db.Find(&stock).GetErrors()

	return stock, err
}

func ListInventory() {

}

func ReleaseItem() {

}

func AddLocation(location migrations.Location) (migrations.Location, []error) {
	db := database.Connect()
	db.LogMode(false)
	err := db.Create(&location).GetErrors()

	return location, err
}

func ListLocations() (locations []migrations.Location, err error) {
	db := database.Connect()

	err = db.Find(&locations).Error

	return locations, err
}

func UpdateLocation(location migrations.Location) (migrations.Location, []error) {

	db := database.Connect()

	var loc migrations.Location

	db.First(&loc, location.ID)

	loc.Name = location.Name
	errors := db.Save(&loc).GetErrors()

	return loc, errors
}
