package migrations

import "../database"

func Migrate() {

	db := database.Connect()

	db.AutoMigrate(&Item{}, &Location{}, &Delivery{}, &Stock{}, &Receipt{}, &Inventory{})

}

func DropTables() {

	db := database.Connect()

	db.DropTableIfExists(&Item{}, &Location{}, &Delivery{}, &Stock{}, &Receipt{}, &Inventory{})

	database.CloseConnection(db)
}
