package migrations

import (
	"github.com/jinzhu/gorm"
	"time"
)

type Inventory struct {
	gorm.Model
	ItemID        uint     `json:"item_id"`
	Item          Item     `json:"item"`
	Quantity      float64  `json:"quantity"`
	Direction     string   `json:"direction"`
	LocationID    uint     `json:"location_id"`
	Location      Location `json:"location"`
	ReceiverID    uint     `json:"receiver_id"`
	ReleaserID    uint     `json:"releaser_id"`
	Cost_Per_Unit float64  `json:"cost_per_unit"`
	DeliveryID    uint     `json:"delivery_id"`
	Delivery      Delivery `json:"-"`
}

type Stock struct {
	gorm.Model
	ItemID         uint      `gorm:"not null" json:"item_id"`
	QuantityOnHand float64   `gorm:"not null; default:0.0" json:"quantity_on_hand"`
	Date           time.Time `gorm:"not null" json:"date"`
}

type Item struct {
	gorm.Model
	Name         string  `gorm:"unique;not null" json:"name"`
	Unit         string  `gorm:"not null" json:"unit"`
	SmallestUnit float64 `gorm:"not null" json:"smallest_unit"`
}

type Location struct {
	gorm.Model
	Name string `gorm:"unique;not null" json:"name"`
}

type Delivery struct {
	gorm.Model
	SupplierID  uint        `gorm:"not null" json:"supplier_id"`
	Supplier    User        `json:"supplier"`
	Status      bool        `gorm:"default:false" json:"status"`
	Inventories []Inventory `gorm:"foreignkey:DeliveryID" json:"inventories"`
	Receipts    []Receipt   `gorm:"foreignkey:DeliveryID" json:"receipts"`
}

type Receipt struct {
	gorm.Model
	DeliveryID    uint     `sql:"type: INT(10) UNSIGNED REFERENCES deliveries(id)" json:"delivery_id" validate:"required"`
	Delivery      Delivery `json:"-"`
	ReceiptNumber string   `gorm:"unique;not null" json:"receipt_number" validate:"required"`
	Amount        float64  `gorm:"not null" json:"amount" validate:"required"`
	BankName      string   `gorm:"not null" json:"bank_name" validate:"required"`
	AccountNumber string   `gorm:"not null" json:"account_number" validate:"required"`
	AccountName   string   `gorm:"not null" json:"account_name" validate:"required"`
}

type Role struct {
	gorm.Model
	Name        string `json:"name"`
	Description string `json:"description"`
}

type User struct {
	gorm.Model
	FName     string `json:"f_name"`
	LName     string `json:"l_name"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Confirmed bool   `son:"confirmed"`
	Role      Role   `json:"role"`
	RoleID    uint   `json:"role_id"`
}

type DeliveryInfo struct {
	gorm.Model
	SupplierID  uint        `gorm:"not null" json:"supplier_id"`
	Supplier    User        `json:"supplier"`
	Status      bool        `gorm:"default:false" json:"status"`
	Inventories []Inventory `gorm:"foreignkey:DeliveryID" json:"inventories"`
	Receipts    []Receipt   `gorm:"foreignkey:DeliveryID" json:"receipts"`
}
